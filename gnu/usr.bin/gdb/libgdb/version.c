/* $DragonFly: src/gnu/usr.bin/gdb/libgdb/version.c,v 1.3 2008/07/28 22:33:21 corecode Exp $ */
#include "version.h"
const char version[] = "6.7.1";
const char host_name[] = MACHINE_ARCH"-dragonfly";
const char target_name[] = TARGET_ARCH"-dragonfly";
