.\"
.\" Copyright (c) 2008
.\"	The DragonFly Project.  All rights reserved.
.\"
.\" Redistribution and use in source and binary forms, with or without
.\" modification, are permitted provided that the following conditions
.\" are met:
.\"
.\" 1. Redistributions of source code must retain the above copyright
.\"    notice, this list of conditions and the following disclaimer.
.\" 2. Redistributions in binary form must reproduce the above copyright
.\"    notice, this list of conditions and the following disclaimer in
.\"    the documentation and/or other materials provided with the
.\"    distribution.
.\" 3. Neither the name of The DragonFly Project nor the names of its
.\"    contributors may be used to endorse or promote products derived
.\"    from this software without specific, prior written permission.
.\"
.\" THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
.\" ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
.\" LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
.\" FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
.\" COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
.\" INCIDENTAL, SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING,
.\" BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
.\" LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
.\" AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
.\" OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
.\" OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
.\" SUCH DAMAGE.
.\"
.\" $DragonFly: src/share/man/man5/hammer.5,v 1.9 2008/08/21 23:38:03 thomas Exp $
.\"
.Dd July 24, 2008
.Os
.Dt HAMMER 5
.Sh NAME
.Nm HAMMER
.Nd HAMMER file system
.Sh SYNOPSIS
To compile this driver into the kernel,
place the following line in your
kernel configuration file:
.Bd -ragged -offset indent
.Cd options HAMMER
.Ed
.Pp
Alternatively, to load the driver as a
module at boot time, place the following line in
.Xr loader.conf 5 :
.Bd -literal -offset indent
hammer_load="YES"
.Ed
.Pp
To mount via
.Xr fstab 5 :
.Bd -literal -offset indent
/dev/ad0s1d[:/dev/ad1s1d:...]	/mnt hammer rw 2 0
.Ed
.Sh DESCRIPTION
The
.Nm
file system provides facilities to store file system data onto disk devices
and is intended to replace UFS as the default file system for
.Dx .
Among its features are instant crash recovery,
large file systems spanning multiple volumes,
fine grained history retention,
mirroring capability, and pseudo file systems.
For a more detailed introduction refer to the paper listed in the
.Sx SEE ALSO
section.
For some common usages of
.Nm
see the
.Sx EXAMPLES
section below.
.Pp
All functions related to managing
.Nm
file systems are provided by the
.Xr newfs_hammer 8 ,
.Xr mount_hammer 8 ,
.Xr hammer 8 ,
and
.Xr undo 1
utilities.
.Ss Instant Crash Recovery
After a non-graceful system shutdown,
.Nm
file systems will be brought back into a fully coherent state
when mounting the file system, usually within a few seconds.
.Ss Large File Systems & Multi Volume
A
.Nm
file system can span up to 256 volumes.
Each volume occupies a
.Dx
disk slice or partition, or another special file,
and can be up to 4096 TB in size.
For volumes over 2 TB in size
.Xr gpt 8
and
.Xr disklabel64 8
normally need to be used.
.Ss Transaction IDs
The
.Nm
file system uses 64 bit, hexadecimal transaction IDs to refer to historical
file or directory data.
An ID has the format
.Li 0x%016llx ,
such as
.Li 0x00000001061a8ba6 .
.Pp
Related
.Xr hammer 8
commands:
.Ar synctid
.Ss History & Snapshots
History metadata on the media is written with every sync operation, so that
by default the resolution of a file's history is 30-60 seconds until the next
prune operation.
Prior versions of files or directories are generally accessible by appending
.Li @@
and a transaction ID to the name.
The common way of accessing history, however, is by taking snapshots.
.Pp
Snapshots are softlinks to prior versions of directories and their files.
Their data will be retained across prune operations for as long as the
softlink exists.
Removing the softlink enables the file system to reclaim the space
again upon the next prune & reblock operations.
.Pp
Related
.Xr hammer 8
commands:
.Ar history ,
.Ar snapshot ;
see also
.Xr undo 1
.Ss Pruning & Reblocking
Pruning is the act of deleting file system history.
Only history used by the given snapshots and history from after the latest
snapshot will be retained.
All other history is deleted.
Reblocking will reorder all elements and thus defragment the file system and
free space for reuse.
After pruning a file system must be reblocked to recover all available space.
.Pp
Related
.Xr hammer 8
commands:
.Ar reblock ,
.Ar reblock-btree ,
.Ar reblock-inodes ,
.Ar reblock-dirs ,
.Ar reblock-data ,
.Ar prune ,
.Ar prune-everything
.Ss Mirroring & Pseudo File Systems
In order to allow inode numbers to be duplicated on the slaves
.Nm Ap s
mirroring feature uses
.Dq Pseudo File Systems
(PFSs).
A
.Nm
file system supports up to 65535 PFSs.
Multiple slaves per master are supported, but multiple masters per slave
are not.
Slaves are always read-only.
Upgrading slaves to masters and downgrading masters to slaves are supported.
.Pp
Related
.Xr hammer 8
commands:
.Ar pfs-master ,
.Ar pfs-slave ,
.Ar pfs-status ,
.Ar pfs-update ,
.Ar pfs-destroy ,
.Ar pfs-upgrade ,
.Ar pfs-downgrade ,
.Ar mirror-copy ,
.Ar mirror-stream ,
.Ar mirror-read ,
.Ar mirror-read-stream ,
.Ar mirror-write ,
.Ar mirror-dump
.Sh EXAMPLES
.Ss Preparing the File System
To create and mount a
.Nm
file system use the
.Xr newfs_hammer 8
and
.Xr mount_hammer 8
commands.
Note that all
.Nm
file systems must have a unique name on a per-machine basis.
.Bd -literal
newfs_hammer -L Home /dev/ad0s1d
mount_hammer /dev/ad0s1d /home
.Ed
.Pp
Similarly, multi volume file systems can be created and mounted by
specifying additional arguments.
.Bd -literal
newfs_hammer -L MultiHome /dev/ad0s1d /dev/ad1s1d
mount_hammer /dev/ad0s1d /dev/ad1s1d /home
.Ed
.Pp
Once created and mounted,
.Nm
file systems need to be reblocked periodically in order not to fill up
over time, either manually or with a
.Xr cron 8
job.
It is recommended that the
.Xr hammer 8
utility's
.Fl c
and
.Fl t
options be used for this job;
for example, to reblock the
.Pa /home
file system every night at 2:15 for up to 5 minutes:
.Bd -literal
15 2 * * * hammer -c /var/run/Home -t 300 reblock /home >/dev/null 2>&1
.Ed
.Ss Snapshots
The
.Xr hammer 8
utility's
.Ar snapshot
command provides several ways of taking snapshots.
They all assume a directory where snapshots are kept.
.Bd -literal
mkdir /snaps
hammer snapshot /home /snaps/snap1
(...after some changes in /home...)
hammer snapshot /home /snaps/snap2
.Ed
.Pp
The softlinks in
.Pa /snaps
point to the state of the
.Pa /home
directory at the time each snapshot was taken, and could now be used to copy
the data somewhere else for backup purposes.
.Ss Pruning
A snapshot directory is also the argument to the
.Xr hammer 8 Ap s
.Ar prune
command which frees historical data from the file system that is not
pointed to by any snapshot link and is not from after the latest snapshot.
.Bd -literal
rm /snaps/snap1
hammer prune /snaps
.Ed
.Pp
Unless the file system is mounted with the
.Ar nohistory
option, it might be advisable to also set up
.Xr cron 8
jobs for pruning no longer used historical data regularly.
.Ss Mirroring
Mirroring can be set up using
.Nm Ap s
pseudo file systems.
To associate the slave with the master its shared UUID should be set to
the master's shared UUID as output by the
.Nm hammer Ar pfs-master
command.
.Bd -literal
hammer pfs-master /home/master
hammer pfs-slave /home/slave shared-uuid=<master's shared uuid>
.Ed
.Pp
The
.Pa /home/slave
link is unusable for as long as no mirroring operation has taken place.
.Pp
To mirror the master's data, either pipe a
.Fa mirror-read
command into a
.Fa mirror-write
or, as a short-cut, use the
.Fa mirror-copy
command (which works across a
.Xr ssh 1
connection as well).
.Bd -literal
hammer mirror-copy /home/master /home/slave
.Ed
.Sh SEE ALSO
.Xr undo 1 ,
.Xr disklabel64 8 ,
.Xr gpt 8 ,
.Xr hammer 8 ,
.Xr mount_hammer 8 ,
.Xr newfs_hammer 8
.Rs
.%A Matthew Dillon
.%D June 2008
.%T "The HAMMER Filesystem"
.Re
.Sh HISTORY
The
.Nm
file system first appeared in
.Dx 1.11 .
.Sh AUTHORS
.An -nosplit
The
.Nm
file system was designed and implemented by
.An Matthew Dillon Aq dillon@backplane.com .
This manual page was written by
.An Sascha Wildner .
